#include <mpi.h>
#include <Eigen/Dense>
#include <cmath>
#include <vector>
#include "POD.hpp"
#include "mpi_gemm_mw.hpp"
#include "walltime.hpp"

using namespace std;
using namespace Eigen;

typedef Matrix<double, -1, -1, RowMajor>  Mat;

static double run(int m, int n, int bs, int nofworkers){

  int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (rank == 0) {
    WallTime<double> timer;
    Mat A(m,n); A.setRandom();
    Mat corr = Mat::Zero(n,n); 

    timer.tic();
    gemm_master(A, bs, corr, nofworkers);
    double time = timer.toc();

    return time;
  } else if( rank <= nofworkers ) {
    gemm_worker<Mat>();
    return 0.0;
  } else {
    int n, bs;
    MPI_Bcast(&n , 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&bs, 1, MPI_INT, 0, MPI_COMM_WORLD);
  }
  return 0.0;
}

int main(int argc, char** argv) {
  MPI_Init(&argc, &argv);
  int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int nofworkers; MPI_Comm_size(MPI_COMM_WORLD, &nofworkers); --nofworkers;

  assert(nofworkers > 1);

  vector<int> BS{ 50, 100, 500, 1000, 3000,  7000 }; 

  // block that fits in cach 
  int m = 100*7000, n = 100;

  if(rank == 0) cout << "Dimension A m = " << m 
    << " n = " << n 
      << " nofworkers = " << nofworkers << endl;

  if(rank == 0) cout << "Matrix size in MB " << m*n*sizeof(double)/1e6 << endl;
  if(rank == 0) cout << " timeSeriell = ";

  double timeSeriell;
  if(rank == 0) {
    WallTime<double> timer;
    Mat A(m,n); A.setRandom();
    Mat corr(n,n); 
    timer.tic();
    corr = A.transpose() * A;
    timeSeriell = timer.toc();
  }
  if(rank == 0) cout << timeSeriell<< endl;


  if(rank == 0){
    cout << "bs bs-in-kb ";
    for (int i=1; i <= nofworkers; i++) cout << i << " ";
    cout << endl;

    for (vector<int>::iterator it = BS.begin() ; it != BS.end(); ++it){
      int bs = *it;
      if(rank == 0) { cout << bs << " " <<  n*bs*sizeof(double)/1e3 << "  "; }
    }
    cout << endl;
  }
  for (int i=1; i <= nofworkers; i++) {
    if(rank == 0) { cout << i << " " ; }
    for (vector<int>::iterator it = BS.begin() ; it != BS.end(); ++it){
      int bs = *it;

      MPI_Barrier(MPI_COMM_WORLD); // sync
      auto time = run(m,n,bs,i);
      if(rank == 0) {cout << "  " << timeSeriell/time ;}
      MPI_Barrier(MPI_COMM_WORLD); // sync
    }
    if(rank == 0) cout << endl;
  }
  if(rank == 0) cout << endl;

  MPI_Finalize();
}
