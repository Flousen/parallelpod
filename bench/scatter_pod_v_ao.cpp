#include <mpi.h>
#include <Eigen/Dense>
#include "POD.hpp"
#include "walltime.hpp"
#include "pod_ezy_v_ao.hpp"
#include "gemm_naiv.hpp"

#include <list>

#include <vector>  
#include <numeric>

using namespace std;
using namespace Eigen;
typedef Matrix<double, -1, -1, RowMajor> Mat;



int main(int argc, char **argv){
  MPI_Init(&argc, &argv);
  int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);

  int m, n, bs;
  std::size_t pos;
  m = std::stoi(argv[1], &pos);
  n = std::stoi(argv[2], &pos);
  bs= std::stoi(argv[3], &pos);

  Mat W, U;
  std::list<double> l;

  if (rank == 0){
    W = Mat::Random((m/size)*size, n);
    U = Mat((m/size)*size, n);
    std::cout << size << '\t';
    std::cout << m << '\t'<< n << endl;
  }

  l = pod_ezy_v(W ,U, bs);

  //if (rank == 0){
  //  for (double n : l) std::cout << n << '\t';  
  //  cout << endl;
  //}

  MPI_Finalize();
}
