#include <mpi.h>
#include <Eigen/Dense>
#include "POD.hpp"
#include "walltime.hpp"
#include "pod_ezy.hpp"

#include <list>

#include <vector>  
#include <numeric>

using namespace std;
using namespace Eigen;
typedef Matrix<double, -1, -1, RowMajor> Mat;

int main(int argc, char **argv){
  int prov;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &prov);
  //MPI_Init(&argc, &argv);
  int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);

  int m, n;
  std::size_t pos;
  m = std::stoi(argv[1], &pos);
  n = std::stoi(argv[2], &pos);

  Mat W, U;
  std::list<double> l;

  if (rank == 0){
    W = Mat::Random(m, n);
    U = Mat((m/size)*size, n);
  }

  l = pod_ezy(W ,U);

  if (rank == 0){
    std::cout << size << '\t';
    std::cout << m << '\t'<< n << '\t';
    for (double val : l) std::cout << val << '\t';  
    cout << endl;
  }

  MPI_Finalize();
}
