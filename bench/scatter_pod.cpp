#include <mpi.h>
#include <Eigen/Dense>
#include "POD.hpp"
#include "walltime.hpp"
#include "pod_ezy.hpp"

#include <list>

#include <vector>  
#include <numeric>

using namespace std;
using namespace Eigen;
typedef Matrix<double, -1, -1, RowMajor> Mat;


int main(int argc, char **argv){
  MPI_Init(&argc, &argv);
  int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);

  int m, n;
  std::size_t pos;
  m = std::stoi(argv[1], &pos);
  n = std::stoi(argv[2], &pos);

  int header = 0;
  if(argc > 3) header = std::stoi(argv[3], &pos);
  cout.precision(8);
  if (rank==0 && header){
    cout  << "size" << '\t';
    cout  << "m" << '\t';
    cout  << "n" << '\t';
    cout  << "tall" << '\t'<<'\t';
    cout  << "scatter" << '\t'<<'\t';
    cout  << "gemm" << '\t'<< '\t';
    cout  << "reduce" << '\t'<< '\t';
    cout  << "EV" << '\t'<< '\t';
    cout  << "Brodcast" << '\t';
    cout  << "gemm" << '\t'<< '\t';
    cout  << "gather" << '\t' << endl;
  }

  Mat W, U;
  std::list<double> l;

  if (rank == 0){
    W = Mat::Random((m/size)*size, n);
    U = Mat((m/size)*size, n);
    std::cout << size << '\t';
    std::cout << m << '\t'<< n << '\t';
  }

  l = pod_ezy(W ,U);

  if (rank == 0){
    for (double val : l) std::cout << val << '\t';  
    cout << endl;
  }

  MPI_Finalize();
}
