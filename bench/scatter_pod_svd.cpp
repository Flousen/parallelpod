#include <mpi.h>
#include <Eigen/Dense>
#include "POD.hpp"
#include "walltime.hpp"
#include <list>

#include <vector>  
#include <numeric>

#include <stdexcept>
#include <string>

using namespace std;
using namespace Eigen;
typedef Matrix<double, -1, -1, RowMajor> Mat;


template <typename MatrixType>
static std::list<double>
pod_par(MatrixType & W, MatrixType & U) {
  std::list<double> l;  WallTime<double> timer;

  WallTime<double> timerAll;
  timerAll.tic();

  int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);
  int m , n , q;

  MatrixType Vhat;
  if (rank == 0){
    m = W.rows(); n = W.cols();
    q = n;
    Vhat = MatrixType(n,n*size);
  }

  MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&q, 1, MPI_INT, 0, MPI_COMM_WORLD);

  int nofrows = m / size;
  int remainder = m % size;
  if (rank < remainder) ++nofrows;
  
  int counts[size]; int displs[size];
  if (rank == 0) {
    int offset = 0;
    for (int i = 0; i < size; ++i) {
      displs[i] = offset; 
      counts[i] = m / size;
      if (i < remainder) ++counts[i];
      
      counts[i] *= n; 
      offset += counts[i];
    }
  }

  MatrixType w( nofrows , n ), sv( n , n ), v( n , n ), CORR( n , n );
  
  Eigen::SelfAdjointEigenSolver<MatrixType> es;
  Eigen::BDCSVD<MatrixType> svd;

  timer.tic();

  MPI_Scatterv( W.data() , counts , displs , MPI_DOUBLE ,
        w.data(), nofrows*n , MPI_DOUBLE ,
        0 , MPI_COMM_WORLD  );
  
  l.push_back(timer.toc()); timer.tic();

  svd.compute(w, Eigen::ComputeThinU | Eigen::ComputeThinV);
  svd.computeV();
  
  l.push_back(timer.toc()); timer.tic();

  MPI_Gather( svd.matrixV().data(), n*n,  MPI_DOUBLE ,
              Vhat.data(), n*n,  MPI_DOUBLE,
              0, MPI_COMM_WORLD);
  
  l.push_back(timer.toc()); timer.tic();

  MatrixType corr = w.transpose() * w;
  
  l.push_back(timer.toc()); timer.tic();

  MPI_Reduce(corr.data(), CORR.data(), n*n, MPI_DOUBLE,
             MPI_SUM, 0, MPI_COMM_WORLD);

  l.push_back(timer.toc()); timer.tic();
  
  if (rank == 0) {
    svd.compute(w, Eigen::ComputeThinU | Eigen::ComputeThinV);
    svd.computeV();
    v = svd.matrixV().leftCols(n);

    CORR = v.transpose() * CORR * v;

    es.compute(CORR);
    // work = V * S^-1
    sv = es.eigenvectors().rowwise().reverse().leftCols(n);
    for(int i=0; i<sv.cols(); i++) {
      sv.col(i) *= 1.0/sqrt(es.eigenvalues().reverse().head(n)[i]);
    }
    
  }
  l.push_back(timer.toc()); timer.tic();

  MPI_Bcast(sv.data(), n*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast( v.data(), n*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  l.push_back(timer.toc()); timer.tic();
  
  //if (rank == 0){ CORR = v.transpose() * CORR * v; }
  //MPI_Bcast(CORR.data(), n*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  MatrixType u = w * v * sv; 
  l.push_back(timer.toc()); timer.tic();
  MPI_Gatherv( u.data() , nofrows*n , MPI_DOUBLE , 
      U.data() , counts ,  displs , MPI_DOUBLE ,
      0 , MPI_COMM_WORLD  );
  l.push_back(timer.toc()); 

  l.push_front( timerAll.toc() );
  return l;
}

int main(int argc, char **argv){
  MPI_Init(&argc, &argv);
  int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);

  int m = 0, n = 0;
  std::size_t pos;
  m = std::stoi(argv[1], &pos);
  n = std::stoi(argv[2], &pos);

  int header = 0;
  if(argc > 3) header = std::stoi(argv[3], &pos);
  cout.precision(8);
  if (rank==0 && header){
    cout  << "size" << '\t';
    cout  << "m" << '\t';
    cout  << "n" << '\t';
    cout  << "tall" << '\t'<<'\t';
    cout  << "scatter" << '\t'<<'\t';
    cout  << "svd" << '\t'<< '\t';
    cout  << "gather" << '\t'<< '\t';
    cout  << "corr" << '\t'<< '\t';
    cout  << "reduce" << '\t';
    cout  << "svd" << '\t'<< '\t';
    cout  << "bcast" << '\t'<< '\t';
    cout  << "gemm" << '\t'<< '\t';
    cout  << "gather" << '\t'<< '\t';
    cout  << "gather" << '\t' << endl;
  }

  Mat W, U;
  std::list<double> l;
  WallTime<double> timer;

  if (rank == 0) {
    W = Mat::Random((m/size)*size, n);
    U = Mat((m/size)*size, n);
  }
  if (size == 1)  {   
    timer.tic();
    Eigen::BDCSVD<Mat> svd;
    svd.compute(W, Eigen::ComputeThinU | Eigen::ComputeThinV);
    svd.computeU();
    double st = timer.toc();
    cout << "time Serial " << st<< endl;
  }

  l = pod_par(W,U);  
  MPI_Barrier(MPI_COMM_WORLD);
  if (rank == 0){
    for (double n : l) std::cout << n << '\t';
    //cout << accumulate(l.begin(), l.end(), 0.0);    
    cout << endl;
  }

  MPI_Finalize();
}
