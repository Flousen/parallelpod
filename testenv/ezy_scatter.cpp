#include <mpi.h>
#include <Eigen/Dense>
#include <unistd.h>
#include "walltime.hpp"
#include <list>

#include <vector>  
#include <numeric>

using namespace std;
using namespace Eigen;
typedef Matrix<double, -1, -1, RowMajor> Mat;

#include "countdispV2.hpp"


int main(int argc, char **argv){
  MPI_Init(&argc, &argv);
  int rank;  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);

  int m = 0, n = 0, bs = 0;
  std::size_t pos;
  bs  = std::stoi(argv[1], &pos);
  n  = std::stoi(argv[2], &pos);
  m =  bs *size;

  int counts[size]; int displs[size];
  int offset = 0;
  for (int i = 0; i < size; ++i) {
    displs[i] = offset; counts[i] = bs * n; offset += counts[i];
  }

  Mat W, U, uw;
  if (rank == 0){
    W = Mat::Random(m, n);
    uw = Mat::Ones(m, n);
  }
  U = Mat(bs, n);

  MPI_Request rS; MPI_Status statusS;

  MPI_Iscatterv( W.data() , counts , displs , MPI_DOUBLE ,
        U.data(), counts[rank] , MPI_DOUBLE ,
        0 , MPI_COMM_WORLD , &rS );
  
  MPI_Wait( &rS, &statusS );
     
  if (rank == 0) cout << "send back=======================================" << endl; 


  MPI_Request rR; MPI_Status statusR;

  MPI_Igatherv( U.data() , counts[rank] , MPI_DOUBLE , 
      uw.data() , counts ,  displs , MPI_DOUBLE ,
      0 , MPI_COMM_WORLD , &rR );

  MPI_Wait( &rR, &statusR );

  if (rank == 0) {
    cout << "===========================================" << endl;
    cout << "Fehler " << (W - uw).norm() << endl; 
  }
  MPI_Finalize();
}
