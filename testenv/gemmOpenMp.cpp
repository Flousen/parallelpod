#include <iostream>
#include <Eigen/Dense>
#include "walltime.hpp"

using namespace std;
using namespace Eigen;


void func(int m, int n){
typedef Eigen::Matrix<double, -1, -1> Matrix;
  Matrix A(m,n); 
  Matrix B(m,n);
  Matrix C(n,n);
  C = A.transpose() * B;
	
}
  
int main(int argc, char **argv) {

  std::size_t m, n;
  std::size_t pos;
  m = std::stoi(argv[1], &pos);
  n = std::stoi(argv[2], &pos);

  typedef Eigen::Matrix<double, -1, -1> Matrix;
  
  WallTime<double> timer;
  Matrix A(m,n); 
  Matrix B(m,n);
  Matrix C(n,n);

  A.setRandom();
  B.setRandom();

  
  timer.tic();
  C = A.transpose() * B;
  double time = timer.toc();
  
  cout << " time: " << time << "Eigen::nbThreads( )" << nbThreads( ) << endl;


}
