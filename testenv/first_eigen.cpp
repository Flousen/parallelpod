#include <iostream>
#include <Eigen/Dense>

typedef Eigen::Matrix<double, -1, -1>  Matrix;

using namespace std;

template<typename MatrixType>
void fun(MatrixType & A){
  typedef typename MatrixType::Scalar Scalar;

  std::cout << "matrix" << std::endl;
  std::cout << A << std::endl;
  std::cout << "sizeof(T) = " << sizeof(Scalar) << std::endl;
}


int main()
{
    int bs = 5;
    int m = 10, n =  5;
    Matrix A(m,n), B(n,m);
    

    Matrix corr = Matrix::Zero(3,3);

    cout << "corr " << endl;
    cout <<  corr   << endl;


    int a = m/bs;
    int b = m%bs;

    cout << "m/bs = " << m << "/" << bs << endl;
    cout <<  a      << endl;
    
    cout << "m%bs = " << m << "%" << bs << endl;
    cout <<  b      << endl;


    //if (!b) fun(A); else  fun(B);
    if (b)
      cout << "Ich bin da" << endl;
}
