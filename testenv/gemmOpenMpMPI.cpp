#include <mpi.h>
#include <iostream>
#include <Eigen/Dense>
#include "walltime.hpp"

using namespace std;
using namespace Eigen;

int main(int argc, char **argv) {

  int prov;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &prov);
  //MPI_Init(&argc, &argv);
  
  int rank;  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);
  

  std::size_t m, n;
  std::size_t pos;
  m = std::stoi(argv[1], &pos);
  n = std::stoi(argv[2], &pos);

  typedef Eigen::Matrix<double, -1, -1> Matrix;

  WallTime<double> timer;
  Matrix A(m,n); 
  Matrix B(m,n);
  Matrix C(n,n);

  A.setRandom();
  B.setRandom();


  timer.tic();
  C = A.transpose() * B;
  double time = timer.toc();

  cout << "rank" << rank << " time: " << time << "Eigen::nbThreads( )" << nbThreads( ) << endl;
  
  MPI_Finalize();

}
