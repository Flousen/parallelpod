#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Core>

#include "walltime.hpp"

#include <string>

using namespace std;
using namespace Eigen;
typedef Matrix<double, -1, -1, RowMajor> Mat;



int main(int argc, char **argv) {

  int m = 0, n = 0;
  std::size_t pos;
  m = std::stoi(argv[1], &pos);
  n = std::stoi(argv[2], &pos);



  Mat A = Mat::Random(m,n);


  WallTime<double> timer;

  Eigen::BDCSVD<Mat> svd;

  timer.tic();
  svd.compute(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
  cout << timer.toc() << endl;

  
  timer.tic();
  svd.computeU();
  cout << timer.toc() << endl;


  Eigen::BDCSVD<Mat> svdd;

  timer.tic();
  svdd.computeU();
  svdd.compute(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
  cout << timer.toc() << endl;



}
