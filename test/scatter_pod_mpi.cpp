#include <mpi.h>
#include <Eigen/Dense>
#include "POD.hpp"
#include "walltime.hpp"

using namespace std;
using namespace Eigen;
typedef Matrix<double, -1, -1, RowMajor> Mat;



int main(int argc, char **argv){
  MPI_Init(&argc, &argv);
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  //Snapshot matrix
  int m = size * 5, n = 3;
  Mat W, CORR, U;
  if (rank == 0){
    W = Mat(m, n);
    CORR = Mat(n,n);
    W.setRandom();
    U = Mat(m, n);
  }

  Eigen::SelfAdjointEigenSolver<Mat> es;

  MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

  Mat w((m/size), n), sv(n,n);    
  MPI_Scatter(W.data(), (m/size)*n, MPI_DOUBLE,
              w.data(), (m/size)*n, MPI_DOUBLE, 0,
              MPI_COMM_WORLD);

  Mat corr = w.transpose() * w;

  MPI_Reduce(corr.data(), CORR.data(), n*n, MPI_DOUBLE,
               MPI_SUM, 0, MPI_COMM_WORLD);

  if (rank == 0){
    es.compute(CORR);
    // work = V * S^-1
    sv = es.eigenvectors().rowwise().reverse().leftCols(n);
    for(int i=0; i<sv.cols(); i++){
      //sv.block(0,i,sv.rows(),1) *= 1.0/sqrt(es.eigenvalues().reverse().head(n)[i]);
      sv.col(i) *= 1.0/sqrt(es.eigenvalues().reverse().head(n)[i]);
    }
  }

  MPI_Bcast(sv.data(), n*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
  Mat u = w * sv; 
  MPI_Gather( u.data(), (m/size)*n,  MPI_DOUBLE,
              U.data(), (m/size)*n,  MPI_DOUBLE,
              0, MPI_COMM_WORLD);

  if (rank == 0){
    Eigen::BDCSVD<Mat> svd;
    svd.compute(W, Eigen::ComputeThinU | Eigen::ComputeThinV);
    svd.computeU();
    
    cout << "=============================" << endl;
    cout <<  svd.matrixU()  << endl;
    cout << "=============================" << endl;
    cout << U  << endl;
    cout << "=============================" << endl;
    double erra = svd.matrixU().norm();
    double errb = U.norm();
    double errc = (svd.matrixU().cwiseAbs() -  U.cwiseAbs() ).norm();
    cout << erra  << endl;
    cout << errb  << endl;
    cout << errc  << endl;
    cout << errc / (erra * errb) << endl;
  }

  MPI_Finalize();
}
