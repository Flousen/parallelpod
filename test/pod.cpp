#include <iostream>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include "POD.hpp"

int main()
{
    std::size_t m = 10, n =  5;
    
    typedef Eigen::Matrix<double, -1, -1, Eigen::ColMajor> Matrix;

    Matrix A(m,n), Ar(m,n), sv(n,n);
    Eigen::BDCSVD<Matrix> svd;

    A.setRandom();

    svd.compute(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
    svd.computeU();
    std::cout << "======================================" << std::endl;
    Ar =  svd.matrixU() *
          svd.singularValues().asDiagonal() * 
          svd.matrixV().transpose() ;
    std::cout << (A - Ar).norm() << std::endl;

    std::cout << "======================================" << std::endl;

    POD<Matrix> pod(A);
    Matrix S = pod.es.eigenvalues().asDiagonal();
    for(int i=0; i<S.rows(); i++) S(i,i) = sqrt(S(i,i));

    Ar =  pod.U * S * pod.es.eigenvectors().transpose() ;
    std::cout << (A - Ar).norm() << std::endl;
     

}
