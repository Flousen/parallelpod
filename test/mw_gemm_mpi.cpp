#include <mpi.h>
#include <Eigen/Dense>
#include "POD.hpp"
#include "mpi_gemm_mw.hpp"
#include "walltime.hpp"

using namespace std;
using namespace Eigen;

typedef Matrix<double, -1, -1, RowMajor>  Mat;


static bool test(int m, int n, int bs, int rank, int nofworkers){
  if (rank == 0) {
    Mat A(m,n); A.setRandom();
    Mat corr = Mat::Zero(n,n); 
    Mat corrTrue(n,n), err(n,n); 

    gemm_master(A, bs, corr, nofworkers);
    corrTrue = A.transpose() * A;
    err = corrTrue - corr;

    if ( err.norm() < 1e-8 ){
      return true;
    } else {
      cout << "error.norm = " << err.norm() << endl;
      return false;
    }

  } else {
    gemm_worker<Mat>();
    return false;
  }
}


int main(int argc, char** argv) {
  MPI_Init(&argc, &argv);
  int rank, nofworkers;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nofworkers); --nofworkers;

  assert(nofworkers > 0);

  int m = 20, n = 10, bs = 7;
  if (test(m,n,bs,rank, nofworkers)) {
    if(rank == 0) cout << "true" << endl; 
  } else {
    if(rank == 0) cout << "false" << endl;
  }
  
  m = 20, n = 10, bs = 10;
  if (test(m,n,bs,rank, nofworkers)) {
    if(rank == 0) cout << "true" << endl; 
  } else {
    if(rank == 0) cout << "false" << endl;
  }

  m = 20, n = 10, bs = 21;
  if (test(m,n,bs,rank, nofworkers)) {
    if(rank == 0) cout << "true" << endl; 
  } else {
    if(rank == 0) cout << "false" << endl;
  }



  MPI_Finalize();
}

