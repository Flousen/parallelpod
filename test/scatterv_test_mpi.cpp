#include <mpi.h>
#include <Eigen/Dense>
#include "POD.hpp"
#include "walltime.hpp"
#include <list>

#include <vector>  
#include <numeric>

using namespace std;
using namespace Eigen;
typedef Matrix<double, -1, -1, RowMajor> Mat;

#include "countdispV2.hpp"


int main(int argc, char **argv){
  MPI_Init(&argc, &argv);
  int rank;  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);

  int m = 0, n = 0, bs = 0;
  std::size_t pos;
  m  = std::stoi(argv[1], &pos);
  n  = std::stoi(argv[2], &pos);
  bs = std::stoi(argv[3], &pos);

  Mat W, U, uw;

  if (rank == 0){
    W = Mat::Random(m, n);
    uw = Mat::Ones(m, n);
  }
  if (rank == 0) cout << "W=======================================" << endl;
  if (rank == 0) cout << W << endl;

  
  countdispV2 L( size, m, n, bs, rank );
  U = Mat( L.getLocalMatirxSize() , n );

  MPI_Scatterv( W.data(),  L.getcounts(),  L.getdispls(), MPI_DOUBLE,
      U.data(), L.getcounts()[rank], MPI_DOUBLE,
      0, MPI_COMM_WORLD);

  Mat corr = Mat::Zero( n , n );

  //MPI_Request r;  MPI_Status status;

  MPI_Request r[size]; MPI_Status status[size];
  

  for (int i = 1; i< L.getnofbs(); i++){
    L.update(i,false);
    MPI_Iscatterv( W.data() , L.getcounts() , L.getdispls() , MPI_DOUBLE ,
        &U( L.getbsstack1() , 0 ) , L.getcounts()[rank] , MPI_DOUBLE ,
        0 , MPI_COMM_WORLD , &r[i] );
    
    // corr += w.block( i*bs , 0 , i*bs , n ).transpose() * w.block( i*bs , 0 , i*bs , n );
    
    MPI_Wait( &r[i] , &status[i] );
  }

  
  MPI_Barrier( MPI_COMM_WORLD  );
  if (rank == 0) cout << "send back=======================================" << endl; 
  L.init();
 
  for (int i = 0; i < L.getnofbs(); i++){
    MPI_Igatherv( &U( L.getbsstack1() , 0 ) , L.getcounts()[rank] , MPI_DOUBLE , 
        uw.data() , L.getcounts() ,  L.getdispls() , MPI_DOUBLE ,
        0 , MPI_COMM_WORLD , &r[i] );

    L.update(i,true); 
    MPI_Wait( &r[i] , &status[i] );

  }
  

  if (rank == 0) cout << "uw=======================================" << endl;
  if (rank == 0) cout << uw << endl;


  if (rank == 0) {
    cout << "===========================================" << endl;
    cout << "Fehler " << (W - uw).norm() << endl; 
  }
  MPI_Finalize();
}
