#include <iostream>
#include <Eigen/Dense>
#include "gemm_naiv.hpp"

using namespace std;
using namespace Eigen;

typedef Eigen::Matrix<double, -1, -1> Matrix;

int main(int argc, char **argv) {
  std::size_t m, n;
  std::size_t pos;
  m = std::stoi(argv[1], &pos);
  n = std::stoi(argv[2], &pos);

  typedef Eigen::Matrix<double, -1, -1> Matrix;

  Matrix W(m,n), corrN(n,n), corrE(n,n);

  corrN.setZero();
  corrE.setZero();

  W.setRandom();

  gemm_naivT(W.block(0,0,m,n), corrN);

  corrE.noalias() = W.transpose() * W;

  cout << "Error: " << (corrN - corrE).norm() << endl;

  #ifdef EIGEN_DONT_PARALLELIZE 
  cout << "we defined it" << endl;
  #endif

}
