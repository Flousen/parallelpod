#include <mpi.h>
#include <Eigen/Dense>
#include "POD.hpp"
#include "walltime.hpp"
#include "pod_ezy_Iv.hpp"

#include <list>

#include <vector>  
#include <numeric>

using namespace std;
using namespace Eigen;
typedef Matrix<double, -1, -1, RowMajor> Mat;


int main(int argc, char **argv){
  MPI_Init(&argc, &argv);
  int rank;  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);

  int m = 0, n = 0, bs = 0;
  std::size_t pos;
  m  = std::stoi(argv[1], &pos);
  n  = std::stoi(argv[2], &pos);
  bs = std::stoi(argv[3], &pos);

  // round
  m = ( ( ( m/size ) / bs  ) * bs ) * size;

  Mat W, U;
  std::list<double> l;
  WallTime<double> timer;

  if (rank == 0){
    W = Mat::Random(m, n);
    U = Mat(m, n);
  }
  Eigen::BDCSVD<Mat> svd;
  
  if (rank == 0){
    timer.tic();
    svd.compute(W, Eigen::ComputeThinU | Eigen::ComputeThinV);
    svd.computeU();
    double st = timer.toc();
    cout << "time Serial " << st<< endl;
  }

  l = pod_ezy_Iv(W ,U, bs);
  MPI_Barrier(MPI_COMM_WORLD);

  if (rank == 0) cout << "=================================================" << endl;

  if (rank == 0) {
    double erra = svd.matrixU().norm();
    double errb = U.norm();
    cout << erra << endl;
    cout << "=================================================" << endl;
    cout << errb << endl;
    cout << "=================================================" << endl;
    cout << (svd.matrixU().cwiseAbs() -  U.cwiseAbs() ).norm()<< endl;
    cout << "=================================================" << endl;
  }
  
  if (rank == 0) cout << timer.toc() << '\t';
  if (rank == 0){

    for (double n : l) std::cout << n << '\t';
    //cout << accumulate(l.begin(), l.end(), 0.0);
    cout << endl;
  }



  MPI_Finalize();
}
