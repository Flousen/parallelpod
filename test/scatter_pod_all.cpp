#include <mpi.h>
#include <Eigen/Dense>
#include "POD.hpp"
#include "walltime.hpp"
#include "pod_ezy.hpp"
#include "pod_ezy_v_ao.hpp"
#include "pod_ezy_I.hpp"
#include "pod_ezy_Iv_ao.hpp"

#include <list>

#include <vector>  
#include <numeric>

using namespace std;
using namespace Eigen;
typedef Matrix<double, -1, -1, RowMajor> Mat;


int main(int argc, char **argv){
  MPI_Init(&argc, &argv);
  int rank;  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);

  int m = 0, n = 0, bs = 0;
  std::size_t pos;
  m  = std::stoi(argv[1], &pos);
  n  = std::stoi(argv[2], &pos);
  bs = std::stoi(argv[3], &pos);


  Mat W, U, UI, UV, UIV;
  std::list<double> l;

  if (rank == 0){
    W = Mat::Random(m, n);
    U   = Mat(m, n);
    UI  = Mat(m, n);
    UV  = Mat(m, n);
    UIV = Mat(m, n);
  }
  Eigen::BDCSVD<Mat> svd;
  
  if (rank == 0){
    svd.compute(W, Eigen::ComputeThinU | Eigen::ComputeThinV);
    svd.computeU();
  }

  if (rank == 0) cout << "=================================================" << endl;
  l = pod_ezy(W ,U);
  MPI_Barrier(MPI_COMM_WORLD);
  if (rank == 0) cout << "I=================================================" << endl;
  l = pod_ezy_I(W ,UI);
  MPI_Barrier(MPI_COMM_WORLD);
  if (rank == 0) cout << "V=================================================" << endl;
  l = pod_ezy_v(W ,UV, bs);
  MPI_Barrier(MPI_COMM_WORLD);
  if (rank == 0) cout << "IV=================================================" << endl;
  l = pod_ezy_Iv(W ,UIV, bs);
  MPI_Barrier(MPI_COMM_WORLD);

  if (rank == 0) cout << "Err=================================================" << endl;

  if (rank == 0) {
    cout << "=================================================" << endl;
    cout << (svd.matrixU().cwiseAbs() -  U.cwiseAbs() ).norm()<< endl;
    cout << "I=================================================" << endl;
    cout << (svd.matrixU().cwiseAbs() -  UI.cwiseAbs() ).norm()<< endl;
    cout << "V=================================================" << endl;
    cout << (svd.matrixU().cwiseAbs() -  UV.cwiseAbs() ).norm()<< endl;
    cout << "IV=================================================" << endl;
    cout << (svd.matrixU().cwiseAbs() -  UIV.cwiseAbs() ).norm()<< endl;
    
  }
  cout << "=================================================" << endl;




  MPI_Finalize();
}
