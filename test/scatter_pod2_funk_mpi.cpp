#include <mpi.h>
#include <Eigen/Dense>
#include "POD.hpp"
#include "walltime.hpp"

using namespace std;
using namespace Eigen;
typedef Matrix<double, -1, -1, RowMajor> Mat;


template <typename MatrixType>
static void
pod_par(MatrixType & W, MatrixType & U) {

  int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);
  int m , n , q;

  MatrixType Vhat;
  if (rank == 0){
    m = W.rows(); n = W.cols();
    q = n;
    Vhat = MatrixType(n,n*size);
  }

  MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&q, 1, MPI_INT, 0, MPI_COMM_WORLD);

  MatrixType w( m/size , n ), sv( n , n ), v( n , n ), CORR( n , n );
  
  Eigen::SelfAdjointEigenSolver<MatrixType> es;
  Eigen::BDCSVD<MatrixType> svd;

  MPI_Scatter(W.data(), (m/size)*n, MPI_DOUBLE,
              w.data(), (m/size)*n, MPI_DOUBLE,
              0, MPI_COMM_WORLD);
  
  if (rank == 0) cout << "=============================" << endl;

  svd.compute(w, Eigen::ComputeThinU | Eigen::ComputeThinV);
  svd.computeV();
  

  if (rank == 0) cout << "=============================" << endl;

  MPI_Gather( svd.matrixV().data(), n*n,  MPI_DOUBLE ,
              Vhat.data(), n*n,  MPI_DOUBLE,
              0, MPI_COMM_WORLD);

  MatrixType corr = w.transpose() * w;
  if (rank == 0) cout << "=============================" << endl;
  MPI_Reduce(corr.data(), CORR.data(), n*n, MPI_DOUBLE,
             MPI_SUM, 0, MPI_COMM_WORLD);

  
  if (rank == 0) {
    svd.compute(w, Eigen::ComputeThinU | Eigen::ComputeThinV);
    svd.computeV();
    v = svd.matrixV().leftCols(n);

    CORR = v.transpose() * CORR * v;

    es.compute(CORR);
    // work = V * S^-1
    sv = es.eigenvectors().rowwise().reverse().leftCols(n);
    for(int i=0; i<sv.cols(); i++) {
      sv.col(i) *= 1.0/sqrt(es.eigenvalues().reverse().head(n)[i]);
    }
    
  }

  MPI_Bcast(sv.data(), n*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast( v.data(), n*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  
  //if (rank == 0){ CORR = v.transpose() * CORR * v; }
  //MPI_Bcast(CORR.data(), n*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  MatrixType u = w * v * sv; 
  MPI_Gather( u.data(), (m/size)*n,  MPI_DOUBLE ,
              U.data(),(m/size)*n,  MPI_DOUBLE,
              0, MPI_COMM_WORLD);

}

int main(int argc, char **argv){
  MPI_Init(&argc, &argv);
  int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);

  //Snapshot matrix
  int m = size * 10, n = 5;
  Mat W, U;
  if (rank == 0){
    W = Mat::Random(m, n); 
    U = Mat(m, n);
  }

  pod_par(W,U);
   
  if (rank == 0){
    Eigen::BDCSVD<Mat> svd;
    svd.compute(W, Eigen::ComputeThinU | Eigen::ComputeThinV);
    svd.computeU();
    cout << "=============================" << endl;
    cout << svd.matrixU() << endl;
    cout << "=============================" << endl;
    cout << U << endl;
    cout << "=============================" << endl;
    double erra = svd.matrixU().norm();
    double errb = U.norm();
    double errc = ( svd.matrixU().cwiseAbs() - U.cwiseAbs() ).norm();
    cout << erra << endl;
    cout << errb << endl;
    cout << errc << endl;
    cout << errc / ( erra * errb ) << endl;
  }

  MPI_Finalize();
}
