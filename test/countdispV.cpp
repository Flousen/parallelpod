#include <iostream>
#include "countdispV2.hpp"
using namespace std;

int main(int argc, char **argv)
{
  int size = 4;
  int rank = 0;

  int m = 0, n = 0, bs = 0;
  std::size_t pos;
  m  = std::stoi(argv[1], &pos);
  n  = std::stoi(argv[2], &pos);
  bs = std::stoi(argv[3], &pos);

  countdispV2 L(size, m, n, bs, rank);
  int * ptr;

  ptr = L. getdispls();
  for(int i=0; i<size; i++) cout << " " << ptr[i];
  cout << endl;
  ptr = L. getdispls();

  for(int i=0; i<size; i++) cout << " " << ptr[i];
  cout << endl;

  return 0;
}
