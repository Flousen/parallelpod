#include <cassert>
#include <cstdlib>
#include <mpi.h>
#include <Eigen/Dense>
#include "POD.hpp"
#include "get_type.hpp"

#include <memory>
#include <type_traits>


int main(int argc, char** argv) {
  MPI_Init(&argc, &argv);

  int nof_processes; MPI_Comm_size(MPI_COMM_WORLD, &nof_processes);
  int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  assert(nof_processes == 2);

  std::size_t m = 10, n =  5, bs = 2;

  //typedef Eigen::Matrix<int, -1, -1, Eigen::ColMajor> Matrix;
  typedef Eigen::Matrix<int, -1, -1, Eigen::RowMajor> Matrix;
  
  Matrix A(m,n), B(bs,bs);
  MPI_Datatype datatype_A = get_Mtype(A.block(1,1,bs,bs));
  MPI_Datatype datatype_B = get_Mtype(B);

  if (rank == 0) {
    A.setRandom();
    
    std::cout << "A" << std::endl;
    std::cout <<  A  << std::endl;

    std::cout << "A" << std::endl;
    std::cout <<  A.block(1,1,bs,bs)  << std::endl;

    MPI_Send(A.block(1,1,bs,bs).data(), 1, datatype_A, 1, 0, MPI_COMM_WORLD);

  } else {
    MPI_Status status;
    MPI_Recv(B.data(), 1, datatype_B, 0, 0, MPI_COMM_WORLD, &status);
    
    std::cout << "B" << std::endl;
    std::cout <<  B  << std::endl;

  }
  MPI_Finalize();
}
