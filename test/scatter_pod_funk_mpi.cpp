#include <mpi.h>
#include <Eigen/Dense>
#include "POD.hpp"
#include "walltime.hpp"

using namespace std;
using namespace Eigen;
typedef Matrix<double, -1, -1, RowMajor> Mat;

template <typename MatrixType>
static void
pod_ezy(MatrixType & W, MatrixType & U) {

  int rank;  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);
  int m , n , q;
  MatrixType CORR;

  if (rank == 0){
    m = W.rows(); n = W.cols();
    q = n;
    CORR = MatrixType(n,n);
  }

  MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&q, 1, MPI_INT, 0, MPI_COMM_WORLD);

  Eigen::SelfAdjointEigenSolver<MatrixType> es;

  MatrixType w((m/size), n);
  MatrixType sv(n,q);

  MPI_Scatter(W.data(), (m/size)*n, MPI_DOUBLE,
      w.data(), (m/size)*n, MPI_DOUBLE, 0,
      MPI_COMM_WORLD);

  MatrixType corr = w.transpose() * w;

  MPI_Reduce(corr.data(), CORR.data(), n*n, MPI_DOUBLE,
      MPI_SUM, 0, MPI_COMM_WORLD);

  if (rank == 0){
    es.compute(CORR);
    // work = V * S^-1
    sv = es.eigenvectors().rowwise().reverse().leftCols(q);
    for(int i=0; i<sv.cols(); i++){
      sv.col(i) *= 1.0/sqrt(es.eigenvalues().reverse().head(q)[i]);
    }
  }

  MPI_Bcast(sv.data(), n*q, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  MatrixType u = w * sv; 
  MPI_Gather( u.data(), (m/size)*n,  MPI_DOUBLE ,
      U.data(),(m/size)*n,  MPI_DOUBLE,
      0, MPI_COMM_WORLD);

}

int main(int argc, char **argv){
  MPI_Init(&argc, &argv);
  int rank;  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);

  //Snapshot matrix
  int m = size * 5, n = 3;
  Mat W, U;
  if (rank == 0){
    W = Mat::Random(m, n);
    U = Mat(m, n);
  }

  pod_ezy(W ,U);   

  if (rank == 0){
    Eigen::BDCSVD<Mat> svd;
    svd.compute(W, Eigen::ComputeThinU | Eigen::ComputeThinV);
    svd.computeU();
    
    cout << "=============================" << endl;
    cout <<  svd.matrixU()  << endl;
    cout << "=============================" << endl;
    cout << U  << endl;
    cout << "=============================" << endl;
    double erra = svd.matrixU().norm();
    double errb = U.norm();
    double errc = (svd.matrixU().cwiseAbs() -  U.cwiseAbs() ).norm();
    cout << erra  << endl;
    cout << errb  << endl;
    cout << errc  << endl;
    cout << errc / (erra * errb) << endl;
  }

  MPI_Finalize();
}
