
%%
clear all; close all; clc;

np = 4;
N = 1000;
n = N * np; 
L = 3; 
W = randi(100, n, L);

q = L ;


Wp = cell(1,np);

for i=0:np-1
  Wp{i+1} = W((i*N)+1:(i+1)*N,:);
end

U = cell(1,np); S = cell(1,np); V = cell(1,np);
for i = 1:np
  [ ~ , ~ , V{i} ] = svd( Wp{i}, 'econ' );
end


Vhat = [];
for i = 1:np
  Vhat = [Vhat V{i}];
end

[ ~ , ~ , v1 ] = svd( Vhat, 'econ' );
%%
v1=v1(1:L,:);
A = W*v1';


[Ze,Se] = eig(A'*A); Se = sqrt(Se);
Se = diag(Se); [~,I]=sort(Se,'descend'); Se= diag(Se(I)); Ze = Ze(:,I);
uhat = A*Ze*diag(1./diag(Se));




[ UTRUE , STRUE , VTRUE ] = svd( W , 'econ');

UTRUE = ( sign(UTRUE(1,:)).*sign(uhat(1,:)) ).*UTRUE; %fix sign for testing
norm(UTRUE-uhat)/(norm(UTRUE)*norm(uhat))








