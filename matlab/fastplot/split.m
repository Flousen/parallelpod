function [s1,s2] = split(x)
  
  half = length(x)/2;

  s1 = x(1:half);
  s2 = x(half + 1 : end);
end

