clear all; clc;
m = 1e8; mref = m + 3840;

v=[];

for size=1:80
  for bs = 1:100
    mf = ( floor (  floor (  mref/size ) / bs  ) * bs ) * size;
    v=[v m-mf];
  end
end
max(abs(v))
min(abs(v))