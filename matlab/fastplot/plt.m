clear all; close all; clc;
load('matlab.mat');

p = 1:20;
figure(1)
plot(p, pod1(1)./pod1);
hold on; 
plot(p, pod1(1)./pod2);
plot(p,p);
legend('correlation Matrix','alg from paper[1]','optimum','Location','northwest')
xlabel('nof porcessors')
ylabel('speed up')

figure(2)
plot(p, pod2(1)./pod2);
hold on; plot(p,p);
title('alg from paper[1]')
xlabel('nof porcessors')
ylabel('speed up')


