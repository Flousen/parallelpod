clear all; close all; clc;
load('mioDibBS.mat')

bs100  = rmmissing(mioDibBS(1,:));
bs500  = rmmissing(mioDibBS(2,:));
bs1000 = rmmissing(mioDibBS(3,:));
bs5000 = rmmissing(mioDibBS(4,:));
bs10000= rmmissing(mioDibBS(5,:));