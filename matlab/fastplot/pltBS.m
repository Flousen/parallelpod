clear all; close all; clc;

%BS1 = importfile("C:\Users\flo\Code\parallelpod\bench\dat\improved3withBS1.dat", [1, Inf]);
BS10 = importfile("C:\Users\flo\Code\parallelpod\bench\dat\improved3withBS10.dat", [1, Inf]);
BS100 = importfile("C:\Users\flo\Code\parallelpod\bench\dat\improved3withBS100.dat", [1, Inf]);
BS1000 = importfile("C:\Users\flo\Code\parallelpod\bench\dat\improved3withBS1000.dat", [1, Inf]);
%BS10000 = importfile("C:\Users\flo\Code\parallelpod\bench\dat\improved3withBS10000.dat", [1, Inf]);

BS50 = importfile("C:\Users\flo\Code\parallelpod\bench\dat\improved3withBS50.dat", [1, Inf]);
BS500 = importfile("C:\Users\flo\Code\parallelpod\bench\dat\improved3withBS500.dat", [1, Inf]);
BS5000 = importfile("C:\Users\flo\Code\parallelpod\bench\dat\improved3withBS5000.dat", [1, Inf]);

noBS = importfile("C:\Users\flo\Code\parallelpod\bench\dat\improvedwithNoBS.dat", [1, Inf]);
noBS = noBS(2:end);
figure(1)
plot( 1:20 , 1:20 ); hold on;
plot( 1:20 , noBS(1)./noBS  )
%plot( 1:20 , noBS(1)./BS10  )
plot( 1:20 , noBS(1)./BS50 , 'o')
plot( 1:20 , noBS(1)./BS100 , 'x')
plot( 1:20 , noBS(1)./BS500 , '-o')
plot( 1:20 , noBS(1)./BS1000, '-+' )
plot( 1:20 , noBS(1)./BS5000, '-*' )
%plot( 1:20 , noBS(1)./BS10000 , 'x')



legend('opt','no','50','100','500','1000','5000')




