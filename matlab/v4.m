
%%
clear all; close all; clc;

np = 4;
N = 1000;
n = N * np; 
L = 10; 
W = rand(n, L);

q = L ;


Wp = cell(1,np);

for i=0:np-1
  Wp{i+1} = W((i*N)+1:(i+1)*N,:);
end

U = cell(1,np); S = cell(1,np); V = cell(1,np);
for i = 1:np
  [ U{i} , S{i} , V{i} ] = svd( Wp{i}, 'econ' );
  WW{i} = Wp{i}' * Wp{i};
end


Vhat = [];
for i = 1:np
  Vhat = [Vhat V{i}];
end

corre = WW{1};
for i = 2:np
  corre = corre + WW{i};
end

[ t , m , v1 ] = svd( Vhat, 'econ' );
v1=v1(1:L,:);

[ UTRUE , STRUE , VTRUE ] = svd( W , 'econ');

err = [];
for j = 1:10

  A1 = v1'*corre*v1;
  [Ze,Se] = eig(A1); Se = sqrt(Se);
  Se = diag(Se); [~,I]=sort(Se,'descend'); Se= diag(Se(I)); Ze = Ze(:,I);

  U=[];
  for i=1:np
    Up{i} = Wp{i}*v1*Ze/Se;
    U = [U;Up{i}];
  end
  
  UTRUE = ( sign(UTRUE(1,:)).*sign(U(1,:)) ).*UTRUE; %fix sign for testing
  err= [err norm(UTRUE-U)/(norm(UTRUE)*norm(U))];


  
  E=zeros(L);
  for i = 1:np
    Ep{i} = Up{i}' * Wp{i} ;
    E = E + Ep{i};
  end
  
  [ ~ , ~ , v1 ] = svd( E, 'econ' );

end



UTRUE = ( sign(UTRUE(1,:)).*sign(U(1,:)) ).*UTRUE; %fix sign for testing
err= [err norm(UTRUE-U)/(norm(UTRUE)*norm(U))]











