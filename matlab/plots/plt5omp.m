function ezyOMP = plt5omp(filename, dataLines)
%IMPORTFILE Import data from a text file
%  EZYOMP = IMPORTFILE(FILENAME) reads data from text file FILENAME for
%  the default selection.  Returns the numeric data.
%
%  EZYOMP = IMPORTFILE(FILE, DATALINES) reads data for the specified row
%  interval(s) of text file FILENAME. Specify DATALINES as a positive
%  scalar integer or a N-by-2 array of positive scalar integers for
%  dis-contiguous row intervals.
%
%  Example:
%  ezyOMP = plt5omp("C:\Users\flo\Code\parallelpod\dat\plt5\ezyOMP.dat", [2, Inf]);
%
%  See also READTABLE.
%
% Auto-generated by MATLAB on 03-Mar-2020 10:30:13

%% Input handling

% If dataLines is not specified, define defaults
if nargin < 2
  dataLines = [2, Inf];
end

%% Setup the Import Options
opts = delimitedTextImportOptions("NumVariables", 11);

% Specify range and delimiter
opts.DataLines = dataLines;
opts.Delimiter = "\t";

% Specify column names and types
opts.VariableNames = ["nodes4ppn20m1000000n300", "VarName2", "VarName3", "VarName4", "VarName5", "VarName6", "VarName7", "VarName8", "VarName9", "VarName10", "VarName11"];
opts.VariableTypes = ["double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double"];
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";

% Import the data
ezyOMP = readtable(filename, opts);

%% Convert to output type
ezyOMP = table2array(ezyOMP);
end