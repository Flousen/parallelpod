clear all; close all; clc;

paper = plt1paper("C:\Users\flo\Code\parallelpod\dat\plt1\paper.dat", [3, Inf]);
ezyOMP = plt5omp("C:\Users\flo\Code\parallelpod\dat\plt5\ezyOMP.dat", [2, Inf]);

ezyH = plt1ezyH("C:\Users\flo\Code\parallelpod\dat\plt1\ezyH.dat", [3, Inf]);
ezyOMPH = plt1ezyOMPH("C:\Users\flo\Code\parallelpod\dat\plt1\ezyOMPH.dat", [3, Inf]);

figure(1)
hold on;
plot(ezyH(:,1),ezyH(:,1) , 'DisplayName', 'optimum');

plot(ezyH(:,1), sum(ezyH(1,6:10),2)./sum(ezyH(:,6:10),2) , 'DisplayName', 'easy'  );
%plot(ezyH(:,1), ezyH(1,4)./paper(:,4) ,'DisplayName', 'paper' );
plot(ezyOMPH(:,1), sum(ezyOMPH(1,6:10),2)./sum(ezyOMPH(:,6:10),2), 'DisplayName', 'omp'  );

xlabel('Number of Processors')
ylabel('Speed up')
title ('Matrix size m=1e6 , n=300')

legend()