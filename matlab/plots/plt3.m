clear all; close all; clc;

paper = plt1paper("C:\Users\flo\Code\parallelpod\dat\plt1\paper.dat", [3, Inf]);

ezyH = plt1ezyH("C:\Users\flo\Code\parallelpod\dat\plt1\ezyH.dat", [3, Inf]);
ezyOMPH = plt1ezyOMPH("C:\Users\flo\Code\parallelpod\dat\plt1\ezyOMPH.dat", [3, Inf]);
BS40 = plt3BS("C:\Users\flo\Code\parallelpod\dat\plt3\BS40.dat", [2, Inf]);
BS100 = plt3BS("C:\Users\flo\Code\parallelpod\dat\plt3\BS100.dat", [2, Inf]);
BS200 = plt3BS("C:\Users\flo\Code\parallelpod\dat\plt3\BS200.dat", [2, Inf]);
BS300 = plt3BS("C:\Users\flo\Code\parallelpod\dat\plt3\BS300.dat", [2, Inf]);
BS330 = plt3BS("C:\Users\flo\Code\parallelpod\dat\plt3\BS330.dat", [2, Inf]);
BS400 = plt3BS("C:\Users\flo\Code\parallelpod\dat\plt3\BS400.dat", [2, Inf]);

figure(1)
hold on;
%plot(ezyH(:,1),ezyH(:,1), 'DisplayName', 'optimum');

plot(ezyH(:,1), ezyH(1,4)./ezyH(:,4) , 'DisplayName', 'easy' );
%plot(ezyH(:,1), ezyH(1,4)./paper(:,4) , 'DisplayName', 'paper' );
plot(ezyOMPH(:,1), ezyH(1,4)./ezyOMPH(:,4) , 'DisplayName', 'omp' );

plot(ezyH(:,1), ezyH(1,4)./BS40(:,5) , 'DisplayName', 'BS40' );
plot(ezyH(:,1), ezyH(1,4)./BS100(:,5) , 'DisplayName', 'BS100' );
plot(ezyH(:,1), ezyH(1,4)./BS200(:,5) , 'DisplayName', 'BS200' );
plot(ezyH(:,1), ezyH(1,4)./BS300(:,5) , 'DisplayName', 'BS300' );
plot(ezyH(:,1), ezyH(1,4)./BS330(:,5) , 'DisplayName', 'BS330' );
plot(ezyH(:,1), ezyH(1,4)./BS400(:,5) , 'DisplayName', 'BS400' );



xlabel('Number of Processors')
ylabel('Speed up')
title ('Matrix size m=1e6 , n=300')

legend()