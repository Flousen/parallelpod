
%%
clear all; close all; clc;

np = 2;
N = 100;
n = N * np; 
L = 3; 
W = randi(100, n, L);


%%

W1 = W(1:N,:)
W2 = W(N+1:end,:)

[ U1 , S1 , V1 ]=svd( W1, 'econ' );
[ U2 , S2 , V2 ]=svd( W2, 'econ' );


Vhat = [V1 V2]

[ u1 , s1 , v1 ] = svd( Vhat, 'econ' );

A = W*v1'

[ uhat , s , z ] = svd(A , 'econ' );
uhat = uhat(:,1:3)


a1 =  W1*v1'
a2 =  W2*v1'
[ ev1 , ew1 ] = eig( a1'*a1 );
[ ev2 , ew2 ] = eig( a2'*a2 );


u1 = W1*ev1(4:6,4:6)/ew1(4:6,4:6)
u2 = W1*ev2(4:6,4:6)/ew2(4:6,4:6)

u = [u1; u2]

%%

[ U , S , V ] = svd( W , 'econ');
[ ev , ew ] = eig( W' * W ); ew = sqrt( ew );





