#ifndef POD_EZY_IV_AO_H
#define POD_EZY_IV_AO_H

#include <mpi.h>
#include <Eigen/Dense>
#include "POD.hpp"
#include "walltime.hpp"
#include "gemm_naiv.hpp"
#include <iostream>
#include <list>
#include <unistd.h>

#include <vector>  
#include <numeric>

using namespace std;
using namespace Eigen;

#include "countdispV2.hpp"

#include <algorithm>

template <typename MatrixType>
static std::list<double>
pod_ezy_Iv(MatrixType & W, MatrixType & U, int bs) {
  int rank;  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);

  std::list<double> l; 
  
  WallTime<double> timerG;
  WallTime<double> timerAll;
  timerAll.tic();


  int m , n , q;

  MatrixType CORR;

  if (rank == 0){
    m = W.rows(); n = W.cols();
    q = n;
    CORR = MatrixType(n,n);
  }

  MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

  Eigen::SelfAdjointEigenSolver<MatrixType> es;

  countdispV2 L( size, m, n, bs, rank );


  MatrixType w(L.getLocalMatirxSize(), n);
  MatrixType sv;

  MPI_Request r[L.getnofbs()]; MPI_Status status[L.getnofbs()];
  MatrixType corr = MatrixType::Zero(n,n);
  
  MPI_Iscatterv( W.data(),  L.getcounts(),  L.getdispls(), MPI_DOUBLE,
      w.data(), L.getcounts()[rank], MPI_DOUBLE,
      0, MPI_COMM_WORLD, &r[0]);



  for (int i = 1; i<  L.getnofbs(); i++){
    L.update(i,false);    
    MPI_Iscatterv( W.data(), L.getcounts(),  L.getdispls(), MPI_DOUBLE,
        &w(L.getbsstack1() ,0), L.getcounts()[rank], MPI_DOUBLE,
        0, MPI_COMM_WORLD, &r[i]);

    timerG.tic();  //===================================
    MPI_Wait( &r[i-1] , &status[i-1] );
    l.push_back(timerG.toc());  //===================================

    //gemm_naivT(w.block( L.getbsstack2() , 0 , L.getbs2() , n ), corr);
    corr.noalias() += w.block( L.getbsstack2() , 0 , L.getbs2() , n ).transpose() 
          * w.block( L.getbsstack2() , 0 , L.getbs2() , n );
    
  }
  
  timerG.tic();
  MPI_Wait( &r[L.getnofbs()-1] , &status[L.getnofbs()-1] );
  l.push_back(timerG.toc());

  //gemm_naivT(w.block( L.getbsstack1() , 0 , L.getbs1() , n ), corr);
  corr.noalias() += w.block( L.getbsstack1() , 0 , L.getbs1() , n ).transpose() 
        * w.block( L.getbsstack1() , 0 , L.getbs1() , n );
  

  MPI_Reduce( corr.data(), CORR.data(), n*n, MPI_DOUBLE,
      MPI_SUM, 0, MPI_COMM_WORLD);

  
  if (rank == 0){
    es.compute(CORR);
    // work = V * S^-1
    sv = es.eigenvectors().rowwise().reverse().leftCols(q);
    for(int i=0; i<sv.cols(); i++){
      sv.col(i) *= 1.0/sqrt(es.eigenvalues().reverse().head(q)[i]);
    }
  }
  

  MPI_Bcast(&q, 1, MPI_INT, 0, MPI_COMM_WORLD);
  if (rank != 0) sv = MatrixType(n,q);
  MPI_Bcast(sv.data(), n*q, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  
  L.init();
  MatrixType u(L.getLocalMatirxSize(),q);
 
  for (int i = 0; i < L.getnofbs(); i++){
    u.block( L.getbsstack1() , 0 , L.getbs1() , q) = 
        w.block( L.getbsstack1() , 0 , L.getbs1() , n) * sv; 

    MPI_Igatherv( &u( L.getbsstack1(),0),  L.getcounts()[rank], MPI_DOUBLE, 
        U.data(), L.getcounts(),  L.getdispls(), MPI_DOUBLE, 
        0, MPI_COMM_WORLD, &r[i] );
    L.update(i,true);
  }

  MPI_Waitall( L.getnofbs(), r, status);
  double tall = timerAll.toc() ;


  for (int ii=0; ii < size; ii++){
    if (ii==rank){
      cout << " rank = " << rank;
      cout << " tall = " << tall; 
      cout << " min = " << *std::min_element(l.begin(), l.end());
      cout << " max = " << *std::max_element(l.begin(), l.end());
      cout << endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    sleep(0.1);
  }


  return l;
}

#endif