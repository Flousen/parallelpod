#ifndef SRC_COUNTDISPV_HPP
#define SRC_COUNTDISPV_HPP


class countdispV {
  private:
    int * counts;
    int * displs;
    int nofrows;
    int size;
    int m, n;
    int nofbs, bs;
    int rank;

  public:
    countdispV(int size, int m, int n, int bs, int rank) :
              size{size} , m{m}, n{n}, bs{bs}, rank{rank} { 
      counts = new int[size]; 
      displs = new int[size];
      init();
    } 

    void init() {
      nofrows = m / size;
      int offset = 0;
      for (int i = 0; i < size; ++i) {
        displs[i] = offset; 
        counts[i] = nofrows * n;
        offset += counts[i];
        counts[i] = bs * n;
      }
      nofbs = nofrows/bs;
    }

    int update(int i){
      for (int j = 0; j < size; ++j) displs[j] += counts[j]; 
      return i;
    }

    int * getcounts(){
      return counts;
    }

    int * getdispls(){
      return displs;
    }

    int getnofbs(){
      return nofbs;
    }

    int getbs(){
      return bs;
    }

    ~countdispV(){ 
      delete[] counts; 
      delete[] displs; 
    }
};


#endif 
