#ifndef SRC_COUNTDISPV2_HPP
#define SRC_COUNTDISPV2_HPP

using namespace std;

class countdispV2 {
  private:
    int * counts;
    int * displs;
    int * nofrows;
    int * bsv1;
    int * bsstack1;
    int * bsv2;
    int * bsstack2;

    int rest;
    int nofbs;
    int mloc;

    int size;
    int m, n;
    int  bs;
    int rank;

    bool last;

    void calbs(){
      if ( !last ){
        for (int i = 0; i < size; ++i) {
          counts[i]   = bs;
          nofrows[i] -= bs;
          bsv1[i] = counts[i]; 
          counts[i] *= n;
        }
      } else {
        for (int i = 0; i < size; ++i) {
          counts[i] = nofrows[i];
          bsv1[i] = counts[i]; 
          counts[i] *= n;
        }
      }
    }
    

  public:
    countdispV2(int size, int m, int n, int bs, int rank) :
              size{size} , m{m}, n{n}, bs{bs}, rank{rank} { 
      counts   = new int[size]; 
      displs   = new int[size];
      nofrows  = new int[size];

      bsv1     = new int[size];
      bsstack1 = new int[size];
      bsv2     = new int[size];
      bsstack2 = new int[size];

      init();
    } 

    void init() {
      last = false;
      rest    = m % size;
      int offset = 0;
      int msize = m / size;

      for (int i = 0; i < size; ++i) {
        displs[i] = offset; 
        counts[i] = msize;

        nofrows[i] = (i < rest) ? ++counts[i] : counts[i];
        counts[i] *= n; 
        offset += counts[i];
        bsstack1[i] = 0;
        bsstack2[i] = 0;
      }

      mloc = nofrows[rank];

      nofbs =  msize/bs;
      if ( msize % bs > 0 ) nofbs++;
      if (nofbs == 1) last = true;

      // Blocking ====================================
      calbs();
      
    }

    void update(int index,bool gather){
      
      if (index == nofbs-1 || (gather && index == nofbs-2)) last = true;

      for (int i = 0; i < size; ++i){ 
        // block befor
        bsstack2[i] = bsstack1[i];
        bsv2[i] = bsv1[i];

        // disp
        bsstack1[i] +=  bsv1[i];
        displs[i] += counts[i];  
      }
      calbs();
    }

    int * getcounts(){
      return counts;
    }

    int * getdispls(){
      return displs;
    }

    int getnofbs(){
      return nofbs;
    }

    int getbs1(){
      return bsv1[rank];
    }

    int getbs2(){
      return bsv2[rank];
    }
    
    int getbsstack1(){
      return bsstack1[rank];
    }

    int getbsstack2(){
      return bsstack2[rank];
    }

    int getLocalMatirxSize(){
      return mloc;
    }

    ~countdispV2(){ 
      delete[] counts; 
      delete[] displs; 
    }
};


#endif 
