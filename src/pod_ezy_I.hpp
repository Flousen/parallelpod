#ifndef POD_EZY_I_H
#define POD_EZY_I_H

#include <mpi.h>
#include <Eigen/Dense>
#include "walltime.hpp"
#include <list>

#include <vector>  
#include <numeric>

using namespace std;
using namespace Eigen;

template <typename MatrixType>
static std::list<double>
pod_ezy_I(MatrixType & W, MatrixType & U) {
  std::list<double> l;  WallTime<double> timer;

  WallTime<double> timerAll;
  timerAll.tic();
  timer.tic();

  int rank;  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);
  int m , n , q;
  MatrixType CORR;

  if (rank == 0){
    m = W.rows(); n = W.cols();
    q = n;
    CORR = MatrixType(n,n);
  }

  MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&q, 1, MPI_INT, 0, MPI_COMM_WORLD);

  int nofrows = m / size;
  int remainder = m % size;
  if (rank < remainder) ++nofrows;
  
  int counts[size]; int displs[size];
  if (rank == 0) {
    int offset = 0;
    for (int i = 0; i < size; ++i) {
      displs[i] = offset; 
      counts[i] = m / size;
      if (i < remainder) ++counts[i];
      
      counts[i] *= n; 
      offset += counts[i];
    }
  }

  Eigen::SelfAdjointEigenSolver<MatrixType> es;

  MatrixType w(nofrows, n);
  MatrixType sv(n,q);

  l.push_back(timer.toc()); timer.tic();

  MPI_Request rS; MPI_Status statusS;

  MPI_Iscatterv( W.data() , counts , displs , MPI_DOUBLE ,
        w.data(), nofrows*n , MPI_DOUBLE ,
        0 , MPI_COMM_WORLD , &rS );
  
  l.push_back(timer.toc()); timer.tic();
  
  MPI_Wait( &rS, &statusS );

  l.push_back(timer.toc()); timer.tic();

  MatrixType corr = w.transpose() * w;

  l.push_back(timer.toc()); timer.tic();

  MPI_Reduce(corr.data(), CORR.data(), n*n, MPI_DOUBLE,
      MPI_SUM, 0, MPI_COMM_WORLD);

  l.push_back(timer.toc()); timer.tic();

  if (rank == 0){
    es.compute(CORR);
    // work = V * S^-1
    sv = es.eigenvectors().rowwise().reverse().leftCols(q);
    for(int i=0; i<sv.cols(); i++){
      sv.col(i) *= 1.0/sqrt(es.eigenvalues().reverse().head(q)[i]);
    }
  }

  l.push_back(timer.toc()); timer.tic();

  MPI_Bcast(sv.data(), n*q, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  l.push_back(timer.toc()); timer.tic();

  MatrixType u = w * sv; 

  l.push_back(timer.toc()); timer.tic();

  MPI_Request rR; MPI_Status statusR;
  MPI_Igatherv( u.data() , nofrows*n , MPI_DOUBLE , 
      U.data() , counts ,  displs , MPI_DOUBLE ,
      0 , MPI_COMM_WORLD , &rR );

  l.push_back(timer.toc()); timer.tic();
  
  MPI_Wait( &rR, &statusR );

  l.push_back(timer.toc());
  l.push_front( timerAll.toc() );

  return l;
}

#endif