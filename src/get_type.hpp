#ifndef HPC_MPI_FUNDAMENTAL_HPP
#define HPC_MPI_FUNDAMENTAL_HPP 1

#include <mpi.h>
#include <complex>
#include <type_traits>

#include <Eigen/Dense>

template<typename T> struct fundamental_type {
  static constexpr bool defined = false;
};

template<>
struct fundamental_type<char> {
  static constexpr bool defined = true;
  constexpr MPI_Datatype get() const { return MPI_CHAR; }
};
template<>
struct fundamental_type<signed char> {
  static constexpr bool defined = true;
  constexpr MPI_Datatype get() const { return MPI_SIGNED_CHAR; }
};
template<>
struct fundamental_type<unsigned char> {
  static constexpr bool defined = true;
  constexpr MPI_Datatype get() const { return MPI_UNSIGNED_CHAR; }
};
template<>
struct fundamental_type<short> {
  static constexpr bool defined = true;
  constexpr MPI_Datatype get() const { return MPI_SHORT; }
};
template<>
struct fundamental_type<unsigned short> {
  static constexpr bool defined = true;
  constexpr MPI_Datatype get() const { return MPI_UNSIGNED_SHORT; }
};
template<>
struct fundamental_type<int> {
  static constexpr bool defined = true;
  constexpr MPI_Datatype get() const { return MPI_INT; }
};
template<>
struct fundamental_type<unsigned> {
  static constexpr bool defined = true;
  constexpr MPI_Datatype get() const { return MPI_UNSIGNED; }
};
template<>
struct fundamental_type<long> {
  static constexpr bool defined = true;
  constexpr MPI_Datatype get() const { return MPI_LONG; }
};
template<>
struct fundamental_type<unsigned long> {
  static constexpr bool defined = true;
  constexpr MPI_Datatype get() const { return MPI_UNSIGNED_LONG; }
};
template<>
struct fundamental_type<long long> {
  static constexpr bool defined = true;
  constexpr MPI_Datatype get() const { return MPI_LONG_LONG; }
};
template<>
struct fundamental_type<unsigned long long> {
  static constexpr bool defined = true;
  constexpr MPI_Datatype get() const { return MPI_UNSIGNED_LONG_LONG; }
};
template<>
struct fundamental_type<float> {
  static constexpr bool defined = true;
  constexpr MPI_Datatype get() const { return MPI_FLOAT; }
};
template<>
struct fundamental_type<double> {
  static constexpr bool defined = true;
  constexpr MPI_Datatype get() const { return MPI_DOUBLE; }
};
template<>
struct fundamental_type<long double> {
  static constexpr bool defined = true;
  constexpr MPI_Datatype get() const { return MPI_LONG_DOUBLE; }
};


template<typename T>
typename std::enable_if< fundamental_type<T>::defined, MPI_Datatype>::type
get_type(const T& value) {
  return fundamental_type<T>().get();
}

template<typename MatrixType>
MPI_Datatype get_row_type(const MatrixType & A) {
  typedef typename MatrixType::Scalar Scalar;
  
  MPI_Datatype rowtype;
  MPI_Type_vector(
      /* count              = */ A.cols(),
      /* blocklength        = */ 1,
      /* stride             = */ A.colStride(),
      /* element type       = */ get_type( A(0, 0) ),
      /* newly created type = */ &rowtype );
  if (A.rowStride() == A.cols()) {
    MPI_Type_commit(&rowtype);
    return rowtype;
  }
 
  MPI_Datatype resized_rowtype;
  MPI_Type_create_resized(rowtype, 0, A.rowStride() * sizeof(Scalar), &resized_rowtype);
  MPI_Type_commit(&resized_rowtype);
  MPI_Type_free(&rowtype);
  return resized_rowtype;
}

template<class MatrixType>
MPI_Datatype get_Mtype(const MatrixType & A) {
  MPI_Datatype datatype;
  if (A.colStride() == 1) {
    MPI_Type_vector(
        /* count              = */ A.rows(),
        /* blocklength        = */ A.cols(),
        /* stride             = */ A.rowStride(),
        /* element type       = */ get_type( A(0, 0) ),
        /* newly created type = */ &datatype );
  } else {
    MPI_Datatype rowtype = get_row_type(A);
    MPI_Type_contiguous(A.rows(), rowtype, &datatype);
    MPI_Type_free(&rowtype);
  }
  MPI_Type_commit(&datatype);
  return datatype;
}

#endif
