#ifndef POD_EZY_V_AO_H
#define POD_EZY_V_AO_H

#include <mpi.h>
#include <Eigen/Dense>
#include "POD.hpp"
#include "walltime.hpp"
#include "gemm_naiv.hpp"
#include <list>

#include <vector>  
#include <numeric>

using namespace std;
using namespace Eigen;

#include "countdispV2.hpp"

template <typename MatrixType>
static std::list<double>
pod_ezy_v(MatrixType & W, MatrixType & U, int bs) {
  int rank;  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);

  std::list<double> l; 
  WallTime<double> timerG;
  WallTime<double> timerAll; 
  timerAll.tic();


  int m , n , q;

  MatrixType CORR;

  if (rank == 0){
    m = W.rows(); n = W.cols();
    q = n;
    CORR = MatrixType(n,n);
  }

  MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

  Eigen::SelfAdjointEigenSolver<MatrixType> es;

  countdispV2 L( size, m, n, bs, rank );
  
  MatrixType w(L.getLocalMatirxSize(), n);
  MatrixType sv;

 // MPI_Request r[L.getnofbs()]; MPI_Status status[L.getnofbs()];
  MatrixType corr = MatrixType::Zero(n,n);
  
  timerG.tic();  //==================================
  
  MPI_Scatterv( W.data(),  L.getcounts(),  L.getdispls(), MPI_DOUBLE,
      w.data(), L.getcounts()[rank], MPI_DOUBLE,
      0, MPI_COMM_WORLD);
  
  double first = timerG.toc(); timerG.tic();  //==================================

  for (int i = 1; i<  L.getnofbs(); i++){
    L.update(i,false);
    MPI_Scatterv( W.data(), L.getcounts(),  L.getdispls(), MPI_DOUBLE,
        &w(L.getbsstack1() ,0), L.getcounts()[rank], MPI_DOUBLE,
        0, MPI_COMM_WORLD);

    //MPI_Wait( &r[i-1] , &status[i-1] );
    l.push_back(timerG.toc()); timerG.tic();  //==================================

    //gemm_naivT(w.block( L.getbsstack2() , 0 , L.getbs2() , n ), corr);
    corr.noalias() += w.block( L.getbsstack2() , 0 , L.getbs2() , n ).transpose()
          * w.block( L.getbsstack2() , 0 , L.getbs2() , n );
    l.push_back(timerG.toc()); timerG.tic();  //==================================
  }

  //MPI_Wait( &r[L.getnofbs()-1] , &status[L.getnofbs()-1] );

  //gemm_naivT(w.block( L.getbsstack1() , 0 , L.getbs1() , n ), corr);
  corr.noalias() += w.block( L.getbsstack1() , 0 , L.getbs1() , n ).transpose() 
        * w.block( L.getbsstack1() , 0 , L.getbs1() , n );
  
  double last = timerG.toc();  //===================================

  MPI_Reduce( corr.data(), CORR.data(), n*n, MPI_DOUBLE,
      MPI_SUM, 0, MPI_COMM_WORLD);



  if (rank == 0){
    es.compute(CORR);
    // work = V * S^-1
    sv = es.eigenvectors().rowwise().reverse().leftCols(q);
    for(int i=0; i<sv.cols(); i++){
      sv.col(i) *= 1.0/sqrt(es.eigenvalues().reverse().head(q)[i]);
    }
  }
  

  MPI_Bcast(&q, 1, MPI_INT, 0, MPI_COMM_WORLD);
  if (rank != 0) sv = MatrixType(n,q);
  MPI_Bcast(sv.data(), n*q, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  
  L.init();
  MatrixType u(L.getLocalMatirxSize(),q);
 
  for (int i = 0; i < L.getnofbs(); i++){
    u.block( L.getbsstack1() , 0 , L.getbs1() , q) = 
        w.block( L.getbsstack1() , 0 , L.getbs1() , n) * sv; 

    MPI_Gatherv( &u( L.getbsstack1(),0),  L.getcounts()[rank], MPI_DOUBLE, 
        U.data(), L.getcounts(),  L.getdispls(), MPI_DOUBLE, 
        0, MPI_COMM_WORLD );
    L.update(i,true);
  }

  //MPI_Waitall( L.getnofbs(), r, status);

  double timeall=  timerAll.toc() ;

  if (rank == 0){
    std::cout << timeall << endl;
    std::cout << first << endl;
    int j=0;
    for (double n : l) {
      std::cout << n << '\t';  
      if(++j >= 2) {
        j=0;
        cout << endl;
      }
    }
    cout << last << endl;
  }

  return l;
}

#endif