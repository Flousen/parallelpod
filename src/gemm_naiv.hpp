#ifndef GEMM_NAIV_H
#define GEMM_NAIV_H

#include <Eigen/Dense>
#include "walltime.hpp"
#include <list>

#include <vector>  
#include <numeric>

using namespace std;
using namespace Eigen;

template <typename MatrixType,typename MatrixType2>
void
gemm_naivT(MatrixType && w, MatrixType2 & corr) {
  int n, m;
  n = w.cols();
  m = w.rows();

  for (int j=0; j<m; j++) {
    for (int i=0; i<n; i++) {
      for (int k=0; k<n; k++) {
        corr( i , k ) += w( j , i ) * w( j , k );
      }
    }
  }

}

#endif