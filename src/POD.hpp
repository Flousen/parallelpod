#ifndef PODCLASS_H
#define PODCLASS_H

#include <Eigen/Dense>
#include <Eigen/Eigenvalues> 
#include <math.h>    
#include <iostream>

template <typename MatType>
class POD {
  public:
    MatType corr, U;
    Eigen::SelfAdjointEigenSolver<MatType> es;
    Eigen::BDCSVD<MatType> svd;

    POD(MatType& A){
      reduce(A);
    }

    void reduce(MatType& A){
      corr = (A.transpose()*A);
      es.compute(corr);
  
      // work = V * S^-1
      MatType work = es.eigenvectors();
      for(int i=0; i<work.rows(); i++){
        work.block(0,i,work.rows(),1) *= 1.0/sqrt(es.eigenvalues()[i]);
      }

      // U = A * V*S^-1 = A * work
      U = A * work; 
    }

    void reduceSVD(MatType& A){
      svd.compute(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
      svd.computeU();
      svd.computeU();
      U = svd.matrixU(); 
    }
    
    MatType getModes(){
      return U;
    }
    
    MatType getEigenValues(){
      return es.eigenvalues().real();
    }

    void printStat(){
      std::cout << "corr" << std::endl;
      std::cout <<  corr << std::endl;
      std::cout << "The eigenvalues of A are:" << std::endl;
      std::cout << getEigenValues() << std::endl;
      std::cout << "The matrix of eigenvectors, V, is:" << std::endl;
      std::cout << getModes() << std::endl;
    }
    void hello() { std::cout << "Ciao, fans of POD!" << std::endl; }

};

#endif
