#ifndef MPI_GEMM_MW
#define MPI_GEMM_MW 1

#include <mpi.h>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;

#define FINISH 1 
#define NEXT 2 

template <typename MatrixType>
static void 
gemm_master(MatrixType & A, int bs, 
            MatrixType & corr, int nofworkers) {
  assert( corr.rows() == corr.cols() );
  
  int n = A.cols(); 
  
  // nof chose goot block size 
  MPI_Bcast(&n , 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&bs, 1, MPI_INT, 0, MPI_COMM_WORLD);

  MatrixType buff( n , n );

  corr *= 0;
  
  // nof blocks
  int noftasks  = (int) (A.rows() / bs); 
  int remaining = (int) (A.rows() % bs); 

  // send out initial tasks for all workers
  int next_task = 0;
  for (int worker = 1; worker <= nofworkers; ++worker) {
    if (next_task < noftasks) {
      // pick next remaining task
      MPI_Send( &A( bs*next_task , 0 ) , bs*n , MPI_DOUBLE ,
                worker , NEXT , MPI_COMM_WORLD);
      next_task++; 
    } else {
    
      // there is no work left for this worker
      MPI_Send(0, 0, MPI_DOUBLE, worker, FINISH, MPI_COMM_WORLD);
    }
  }

  int done=0;
  while (done < noftasks) {
    MPI_Status status;
    MPI_Recv(buff.data(), n*n , MPI_DOUBLE,
             MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    int worker = status.MPI_SOURCE;
    
    
    
    if (next_task < noftasks) {
      MPI_Send(&A(bs*next_task, 0), bs* n, MPI_DOUBLE, worker, NEXT,MPI_COMM_WORLD);
      next_task++; 
    } else {
      MPI_Send(0, 0, MPI_DOUBLE, worker, FINISH, MPI_COMM_WORLD);
    }

    corr = corr + buff;
    done++;

  }
  
  if (remaining) {
    buff = A.block( bs*next_task , 0 , remaining , n ).transpose() * A.block( bs*next_task , 0 , remaining , n );
    corr = corr + buff;
  }
  
}

template <typename MatrixType>
static void 
gemm_worker() {
 int n, bs;
  MPI_Bcast(&n , 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&bs, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MatrixType A(bs,n), corr(n,n);

  MPI_Status status;
  for(;;) {
    // receive next task
    MPI_Recv(A.data(), bs*n, MPI_DOUBLE, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    if (status.MPI_TAG == FINISH) break;

    // process it
    corr = A.transpose() * A; 

    // send result back to master
    MPI_Send(corr.data(), n*n, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
  }
}

#endif
