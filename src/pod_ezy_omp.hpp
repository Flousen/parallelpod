#ifndef POD_EZY_H
#define POD_EZY_H


#include <mpi.h>
#include <Eigen/Dense>
#include "walltime.hpp"
#include <list>

#include <vector>  
#include <numeric>

using namespace std;
using namespace Eigen;

template <typename MatrixType>
static std::list<double>
pod_ezy(MatrixType & W, MatrixType & U) {
  std::list<double> l;  WallTime<double> timer;

  WallTime<double> timerAll;
 
  timerAll.tic();

  int rank;  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size; MPI_Comm_size(MPI_COMM_WORLD, &size);
  int m , n , q;
  MatrixType CORR;

  if (rank == 0){
    m = W.rows(); n = W.cols();
    q = n;
    CORR = MatrixType(n,n);
  }

  MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&q, 1, MPI_INT, 0, MPI_COMM_WORLD);

  int nofrows = m / size;
  int remainder = m % size;
  if (rank < remainder) ++nofrows;
  
  int counts[size]; int displs[size];
  if (rank == 0) {
    int offset = 0;
    for (int i = 0; i < size; ++i) {
      displs[i] = offset; 
      counts[i] = m / size;
      if (i < remainder) ++counts[i];
      
      counts[i] *= n; 
      offset += counts[i];
    }
  }

  Eigen::SelfAdjointEigenSolver<MatrixType> es;

  MatrixType w(nofrows, n);
  MatrixType sv(n,q);

  timer.tic();
  MPI_Scatterv( W.data() , counts , displs , MPI_DOUBLE ,
        w.data(), nofrows*n , MPI_DOUBLE ,
        0 , MPI_COMM_WORLD  );
  l.push_back(timer.toc()); timer.tic();

  MatrixType corr;

  corr.noalias() = w.transpose() * w;

  l.push_back(timer.toc()); timer.tic();

  MPI_Reduce(corr.data(), CORR.data(), n*n, MPI_DOUBLE,
      MPI_SUM, 0, MPI_COMM_WORLD);

  l.push_back(timer.toc()); 
  l.push_front( timerAll.toc() );

  return l;
}

#endif
