BIN=./bin/

HEADER     = $(wildcard *.hpp)
TEST_SRC   = $(wildcard *.cpp)
TEST       = $(patsubst %.cpp,%,$(TEST_SRC))

MCC        := mpicc
MCXX       := mpic++

CXX        = g++

CXXFLAGS  :=  -O3 -march=native
CXXFLAGS  += -std=c++11

DIM := 1000000
DIN := 300

CPPFLAGS  := -I ./Eigen -I ./src

#	echo "$(CXX) $(CXXFLAGS) $(CPPFLAGS) $< -o $@"
#	echo "$@: $+"

all:  bench_scatter_pod_OMP

bench_scatter_pod_OMP: bench/scatter_pod_OMP.cpp
	$(MCXX) $(CXXFLAGS) $(CPPFLAGS) -o $(BIN)$@ $<
	$(MCXX) $(CXXFLAGS) $(CPPFLAGS) -fopenmp -o $(BIN)$@omp $<

run:
	mpirun -np 40 $(BIN)bench_scatter_pod_OMP ${DIM} ${DIN} | sed "/cn/d"
	mpirun -np 4 -bind-to socket -map-by node $(BIN)bench_scatter_pod_OMPomp ${DIM} ${DIN} | sed "/cn/d"

test_scatter_pod_all: test/scatter_pod_all.cpp
	$(MCXX) $(CXXFLAGS) $(CPPFLAGS) -o $(BIN)$@ $<
	mpirun -np 2 $(BIN)$@ 101 5 2

test_scatterv_test_mpi: test/scatterv_test_mpi.cpp
	$(MCXX) $(CXXFLAGS) $(CPPFLAGS) -o $(BIN)$@ $<
	mpirun -np 2 $(BIN)$@ 7 5 1

test_scatter_pod_funk_imp_mpi: test/scatter_pod_funk_imp_mpi.cpp
	$(MCXX) $(CXXFLAGS) $(CPPFLAGS) -o $(BIN)$@ $<
	mpirun -np 2 $(BIN)$@ 100 5 2

bench_bench_scatter_pod_funk_imp_mpi: bench/scatter_pod_funk_imp_mpi.cpp
	$(MCXX) $(CXXFLAGS) $(CPPFLAGS) -o $(BIN)$@ $<
	mpirun -np 2 $(BIN)$@ 100 30 5

test_scatter_pod_mpi: test/scatter_pod_mpi.cpp
	$(MCXX) $(CXXFLAGS) $(CPPFLAGS) -o $(BIN)$@ $<

test_scatter_pod_funk_mpi: test/scatter_pod_funk_mpi.cpp
	$(MCXX) $(CXXFLAGS) $(CPPFLAGS) -o $(BIN)$@ $<

bench_scatter_pod_funk_mpi: bench/scatter_pod_funk_mpi.cpp
	$(MCXX) $(CXXFLAGS) $(CPPFLAGS) -o $(BIN)$@ $<

test_scatter_pod2_mpi: test/scatter_pod2_mpi.cpp
	$(MCXX) $(CXXFLAGS) $(CPPFLAGS) -o $(BIN)$@ $<

test_scatter_pod2_funk_mpi: test/scatter_pod2_funk_mpi.cpp
	$(MCXX) $(CXXFLAGS) $(CPPFLAGS) -o $(BIN)$@ $<

bench_scatter_pod2_funk_mpi: bench/scatter_pod2_funk_mpi.cpp
	$(MCXX) $(CXXFLAGS) $(CPPFLAGS) -o $(BIN)$@ $<

test_countdispV: test/countdispV.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -o $(BIN)$@ $<
	$(BIN)$@

test_pod: test/pod.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -o $(BIN)$@ $<

test_naivgemm: test/naivgemm.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -o $(BIN)$@ $<
	$(BIN)$@ 10 5



clean:
	$(RM) core
	$(RM) $(BIN)*
