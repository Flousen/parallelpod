#!/bin/bash
#PBS -q regular
#PBS -l nodes=4:ppn=20
#PBS -l walltime=05:00:00
#PBS -N EV10e6p80

module load openmpi
#change to dir from which you the job was submitted
#cd $PBS_O_WORKDIR 

#make -f conf/makebench

m=1000000
n=300

fileName1="dat/plt3/BS"


for bs in 40 100 200 300 330 400
do
  echo "nodes=4:ppn=20 m = $m n = $n" > ${fileName1}${bs}.dat
done

for np in  $(seq 1 1 80)
do
  for bs in 40 100 200 300 330 400
  do
    mpirun -np ${np} ./bin/bech_scatter_pod_Iv ${m} ${n} ${bs} ${header} | sed "/cn/d" >> ${fileName1}${bs}.dat
  done
  if [ ${header} -eq 1 ]
  then
    header=0;
  fi
done


