#!/bin/bash
#PBS -q regular
#PBS -l nodes=4:ppn=20
#PBS -l walltime=04:00:00
#PBS -N EV10e6p80

module load openmpi
#change to dir from which you the job was submitted
#cd $PBS_O_WORKDIR 

#make -f conf/makebench

m=1000000
n=300

fileName1="dat/plt5/ezyOMP.dat"

echo "nodes=4:ppn=20 m = $m n = $n" > ${fileName1}

for np in  $(seq 1 1 8)
do
  mpirun -np ${np} --bind-to socket --map-by node ./bin/bech_scatter_podomp ${m} ${n} | sed "/cn/d" >> ${fileName1}
done


