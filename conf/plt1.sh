#!/bin/bash
#PBS -q regular
#PBS -l nodes=4:ppn=20
#PBS -l walltime=04:00:00
#PBS -N EV10e6p80

#module load openmpi
#change to dir from which you the job was submitted
#cd $PBS_O_WORKDIR 

#make -f conf/makebench

m=1000000
n=300

fileName1="dat/plt1/ezyH.dat"
fileName2="dat/plt1/paperH.dat"

echo "nodes=4:ppn=20 m = $m n = $n" > ${fileName1}
echo "nodes=4:ppn=20 m = $m n = $n" > ${fileName2}

header=1

for np in  $(seq 1 1 80)
do
  mpirun -np $np ./bin/bech_scatter_pod $m $n ${header}| sed "/cn/d" >> ${fileName1}
#  mpirun -np $np ./bin/bech_scatter_pod_svd $m $n ${header}| sed "/cn/d" >> ${fileName2}

  if [ ${header} -eq 1 ]
  then
    header=0;
  fi
done

fileName3="dat/plt1/ezyOMPH.dat"

echo "nodes=4:ppn=20 m = $m n = $n" > ${fileName3}

header=1
for np in  $(seq 1 1 8)
do
  mpirun -np ${np} --bind-to socket --map-by node ./bin/bech_scatter_podomp ${m} ${n} ${header} | sed "/cn/d" >> ${fileName3}
    if [ ${header} -eq 1 ]
  then
    header=0;
  fi
done

