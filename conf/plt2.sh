#!/bin/bash
#PBS -q regular
#PBS -l nodes=4:ppn=20
#PBS -l walltime=01:00:00
#PBS -N EV10e6p80

module load openmpi
#change to dir from which you the job was submitted
cd $PBS_O_WORKDIR 

make -f conf/makebench

m=1000000
n=300

fileName="dat/plt2/BS10to500omp.dat"

echo "nodes=4:ppn=20" > ${fileName}

header=1
np=8
for bs in  $(seq 10 10 500)
do
  mpirun -np ${np} --bind-to socket --map-by node ./bin/bech_scatter_pod_Ivomp ${m} ${n} ${bs} | sed "/cn/d" >> ${fileName}
  if [ ${header} -eq 1 ]
  then
    header=0;
  fi
done

