set terminal png size 900, 500 background rgb 'white'
set output "a.png"
set xlabel "Number of Prozessors"
set ylabel "parallelTime/SerialTime"
set title "Benchmark gemm"
set key below 
set pointsize 1.2
plot "../dat/c.dat" every ::5 using 1:2 with linespoints lt 2 lw 1 pt 8 ps 1.5 title  "BS = 50  ", \
     "../dat/c.dat" every ::5 using 1:3 with linespoints lt 3 lw 1 pt 7 ps 1.5 title  "BS = 100 ", \
     "../dat/c.dat" every ::5 using 1:4 with linespoints lt 4 lw 1 pt 6 ps 1.5 title  "BS = 500 ", \
     "../dat/c.dat" every ::5 using 1:5 with linespoints lt 5 lw 1 pt 5 ps 1.5 title  "BS = 1000", \
     "../dat/c.dat" every ::5 using 1:6 with linespoints lt 6 lw 1 pt 4 ps 1.5 title  "BS = 3000", \
     "../dat/c.dat" every ::5 using 1:6 with linespoints lt 7 lw 1 pt 3 ps 1.5 title  "BS = 7000", \
     "../dat/c.dat" every ::5 using 1:1 with line lw 3 lc rgb "red" title  "ideal"
